"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const express_1 = __importDefault(require("express"));
const bodyParser = __importStar(require("body-parser"));
const helmet_1 = __importDefault(require("helmet"));
const app = express_1.default();
// var MySQLStore = require('express-mysql-session')(session);
//var sessionStore = new MySQLStore(Env.dbEcrmSession);
// app.use(logger('dev'));
app.use(express_1.default.json());
// app.use(cors());
// need to check the line 16 err  && line 14 err
app.use(helmet_1.default());
app.use(bodyParser.json({ limit: '200mb' }));
app.use(express_1.default.urlencoded({ extended: true, limit: 5e7 }));
app.use(cookie_parser_1.default());
app.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('ok we are here at 21');
    const customers = [
        { id: 1, "firstName": "Nilesh", "lastName": "Mandal" },
        { id: 2, "firstName": "Rohit", "lastName": "chickane" },
        { id: 3, "firstName": "aniket", "lastName": "jadhav" },
        { id: 4, "firstName": "sunil", "lastName": "singh" },
        { id: 5, "firstName": "yash", "lastName": "Tiwari" }
    ];
    res.send(customers);
}));
const port = 3001;
app.listen(port, () => {
    console.log(`server running at http://localhost:${port}`);
});
