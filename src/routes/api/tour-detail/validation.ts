import validator from 'validator';
import Validator from 'validator';
import isEmpty from '../../../shared/isEmpty';

const validate:Function = (inputData:any) => {
    let errors:any = {};

    if(!Validator.isLength(inputData.tourName, { min:2, max:30 }) ){
        errors.name = 'Name must be between 2 to 30 characters';
    }

    if(!Validator.isNumeric(inputData.startTime) && !validator.isLength(inputData.startTime , { min:16, max:16} ) ){
        errors.startTime = 'Start Time must be in milliseconds';
    }

    if(!Validator.isNumeric(inputData.endTime) && !validator.isLength(inputData.endTime , {min:16, max:16} ) ){
        errors.startTime = 'End Time must be in milliseconds';
    }

    if(!Validator.isLength(inputData.travelFood, { min:2 }) ){
        errors.travelFood = 'Travel Food Detail is empty';
    }

    if(!Validator.isLength(inputData.practiseSession, { min:2 }) ){
        errors.practiseSession = 'Practise Session Detail is empty';
    }

    if(!Validator.isLength(inputData.tourMacthes, { min:2 }) ){
        errors.tourMacthes = 'Tour Matches Detail is empty';
    }

    if(!Validator.isLength(inputData.coachDetail, { min:2 }) ){
        errors.coachDetail = 'Coach Detail is empty';
    }

    if(!Validator.isLength(inputData.tourPackage, { min:2 }) ){
        errors.tourPackage = 'Tour Package is empty';
    }

    if(!Validator.isLength(inputData.tourStatus, { min:2 }) ){
        errors.tourStatus = 'Tour status is empty';
    }

    if(!Validator.isLength(inputData.tourLocation, { min:2 }) ){
        errors.tourLocation = 'location detail is incomplete';
    }
    
    if(!Validator.isLength(inputData.tourCapacity, { min:2 }) ){
        errors.tourCapacity = 'Tour Capacity is not mention';
    }

    return {
        errors,
        isValid:isEmpty(errors)
    }
};

export default validate;