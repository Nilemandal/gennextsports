import {Router,Request,Response} from 'express';
import {Queries} from '../../../shared/queries';
const router:any = Router();
const queries:any = new Queries();

/**
 * API to get the tour data 
 * API can access through http://localhost:3001/api/tour/fetch-tour-detail
 * Created By Nile 27/06/2021
 */

router.post('/', async (req:Request,res:Response) => {
    const column:string = `tid,tour_name,tour_detail,tour_start,tour_end,tour_location,tour_capacity,tour_age,travel_detail,travel_food,practise_session,tour_matches,coach_detail,tour_packge,tour_status`;
    const table:string = `tour_detail`;
    const condition:string = `WHERE tour_status = 'active'`;
    let tourRes:any = (await queries.selectQuery(column,table,condition));
    (tourRes[0] && tourRes[0].tid)?
        res.send({status:'success',data:tourRes}):
        res.send({status:'err','msg':"something went wrong"})
});

export default router;