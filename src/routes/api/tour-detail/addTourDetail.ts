import {Router, Request,Response } from 'express';
// import validateInput from './validation';
import {Queries} from './../../../shared/queries';

const router:any = Router();
var queries:any = new Queries();

/**
 * @url http://localhost:3001/api/tour/add-activity
 * @param tourName  name of the tour 
 * @param startTime tour start timestamp
 * @param endTime tour end timestamp
 * @param tourLocation tour location in json with add,city,pincode,state
 * @param tourCapacity Number of strength 
 * @param tourAgeGroup json data with min age n max age for tour
 * @param traveDetail traveling management with bus train flight n detail
 * @param travelFood food detailof tour as breakfast lunch snacks dinner
 * @param practiseSession practise session detail in tour location
 * @param tourMacthes numer matches n detail with opponents in tour like test t20 odi 
 * @param coachDetail coach name who will mentor them for practise match
 * @param tourPackage cost of package n advance payment remaining payment n total payment.
 * @param tourStatus tour status active close etc.
 * @author Nilesh Mandal 
 * @Date 24/01/2021
 */

router.post('/add-activity', async (req:Request,res:Response) => {
    // Validation check
    // const { errors , isValid } = validateInput(req.body);
    // if(!isValid){
    //     return res.status(400).json(errors);
    // }
    let { 
        tourName,
        tourDetail,
        startTime,
        endTime,
        tourLocation,
        tourCapacity,
        tourGroup,
        travelDetail,
        travelFoods,
        practiseSession,
        tourMatch,
        coachDetail,
        tourPackage,
        tourStatus
    } = req.body;

    let data:any = {
        tour_name:tourName,
        tour_detail:tourDetail,
        tour_start:startTime,
        tour_end:endTime,
        tour_location:JSON.stringify(tourLocation),
        tour_capacity:tourCapacity,
        tour_age:tourGroup,
        travel_detail:travelDetail,
        travel_food:travelFoods,
        practise_session:practiseSession,
        tour_matches:tourMatch,
        coach_detail:coachDetail,
        tour_packge:tourPackage,
        tour_status:tourStatus
    }
    const table:string = 'tour_detail';
    let insertQuery:any = await queries.insertQuery(data,table);
    if(insertQuery.insertId > 0){
        res.json({
            status:'success',
            id:insertQuery.insertId
        })
    }else{
        res.json({
            status:'err',
            msg:'something went wrong while query side.'
        })
    }
    
});

export default router;