import { Router,Request,Response } from 'express';
const router = Router();

import addTourDetail from './addTourDetail';
import fetchTourDetail from './fetchTourDetail';

router.use('/tour',addTourDetail);
router.use('/tour/fetch-tour-detail',fetchTourDetail);

export default router;