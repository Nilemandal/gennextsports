import { Router } from "express";
import { authJWT } from "../../../middleware/jwtAuth";
const router = Router();

import getSubscribedBatchData from "./subscribed_data";

router.use('/subcribed-data',[authJWT],getSubscribedBatchData);

export default router;