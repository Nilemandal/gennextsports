import { Router , Request , Response } from "express";
import { Queries } from "../../../shared/queries";

const router = Router();
const queries = new Queries();

router.post('/', async (req:Request,res:Response) => {
    let user = req.body.user;
    const column = `bpid , batch_name , batch_id , batch_expires , timestamp , batch_fees , user_phone , performance`;
    const condition = `WHERE user_id=${user.uid}`
    const table = 'batch_process';
    let resData = await queries.selectQuery(column,table,condition);
    res.send({
        status:'success',
        data:resData
    })
})

export default router;