import Router from 'express';
import rzPayOrderRoute from './razorPay';
import rzPayRes from './razorRes';
import { authJWT } from '../../../middleware/jwtAuth';
const router = Router();

router.use('/payments/razorpay/order-id',[authJWT],rzPayOrderRoute);
router.use('/payments/razorpay/response',[authJWT],rzPayRes);

export default router;