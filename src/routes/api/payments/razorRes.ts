import { Router , Request , Response } from 'express';
import {Queries} from '../../../shared/queries';
import DateUtil from "../../../utils/date";

const router = Router();
const queries = new Queries();

router.post('/', async (req:Request,res:Response)=>{
    let paymentId = req.body.razorpay_payment_id;
    let orderId = req.body.razorpay_order_id;
    let paymentSignature = req.body.razorpay_signature;
    let batchName = req.body.batchName;
    let batchId = req.body.batchId;

    if( paymentId && orderId && paymentSignature){

        let col = 'order_id,payment_amount,product,product_id,user_id,receipt_id,user_phone';
        const table:any = 'payment_order';
        let condition:string = `WHERE order_id = '${orderId}'`;
        let getReceipt = await queries.selectQuery(col,table,condition);
        getReceipt = getReceipt[0];

        let session:any = req.session;
        if( session.user && getReceipt.user_id ){

            let insertObj:any = {
                user_name:session.user.user_name,
                user_phone:session.user.user_phone,
                user_id:session.user.uid,
                product:getReceipt.product,
                product_id:getReceipt.product_id,
                amount:getReceipt.payment_amount,
                timestamp:+new Date(),
                paymentId,
                orderId,
                paymentSignature
            }

            let insertPayment:any = await queries.insertQuery(insertObj,'payment');
            if(insertPayment && insertPayment.insertId){
                
                await paymentProcessUpdate(insertObj,batchName,batchId);

                res.json({
                    status:'success'
                })
            }else{
                res.json({
                    status:'error',
                    reason:'Error Response Handle Time'
                })
            }

        }else{
            res.json({
                status:'error',
                reason:'Error Response Handle Time'
            })
        }

    }else{
        res.json({
            status:'err',
            reason:'Required Parameters Missing'
        })
    }
});

async function paymentProcessUpdate(d:any,bName:string,bId:string){
    let insertQuery = new Object;
    if( d.product == 'cricketBatch'){
        insertQuery = {
            batch_name:bName,
            batch_id:bId,
            timestamp:+new Date(),
            batch_expires: DateUtil.monthStartEndDay(new Date()).endTime,
            batch_fees:d.amount,
            payment_id:d.paymentId,
            payment_gateway:'RazorPay',
            user_id:d.user_id,
            user_phone:d.user_phone
        }
        await queries.insertQuery( insertQuery, 'batch_process' );
    }
}

export default router;