import Router , { Request , Response } from 'express';
import RazorPay from 'razorpay';
import Env from '../../../env';
import { nanoid } from 'nanoid';
import { Queries } from '../../../shared/queries';

var queries = new Queries();

const rzPay = new RazorPay({
    key_id:Env.razorpayId,
    key_secret:Env.razorpayKey
});

const router = Router();
//  [authJWT]
router.post('/', async ( req:Request , res:Response )=>{
    let amount = +(req.body.amount * 100);
    let currency = req.body.currency;
    let notes = (req.body.notes)?req.body.notes:'';
    let { product , productId } = req.body;
    let userSession:any = req.session;
    userSession = userSession.user;

    if( amount && currency ){
        let receiptId:any = nanoid(12);

        const table = 'payment_order';
        const insertData:any = {
            payment_amount:(+amount/100),
            status:'init',
            init_time:+new Date(),
            receipt_id: (receiptId).trim(),
            user_id:userSession.uid,
            user_phone:userSession.user_phone,
            product:product,
            product_id:productId
        }

        let insertRequest:any = await queries.insertQuery(insertData,table);
        if(insertRequest.insertId){
            var options = {
                amount:amount,
                currency: "INR",
                receipt: receiptId,
                notes
            };

            let rzRes:any = '';
            try{
                rzRes = await rzPay.orders.create(options);
            }catch (err){
                console.log('its an err at 55' , err);

                return res.send({ 
                    status:'err',
                    Reason:'Payment initialization Failed'
                });
            }

            if( rzRes && rzRes.id && rzRes.entity == 'order' ){
                
                let updateObj:any = {
                    payment_entity:(rzRes.entity).trim(),
                    payment_amount:+(amount/100),
                    order_id:(rzRes.id).trim(),
                }

                let condition:string = `WHERE receipt_id = '${receiptId}' AND user_id = ${userSession.uid}`;
                let updateQuery:any = await queries.updateQuery(updateObj,table,condition); 

                return res.send({
                    status:'success',
                    data:{
                        orderId:rzRes.id,
                        entity:rzRes.entity,
                        amount,
                        user:{
                            name:userSession.user_name,
                            phone:userSession.user_phone,
                            email:(userSession.user_email)?userSession.user_email:''
                        }
                    }
                })
            }else{
                return res.send({
                    status:'err',
                    reason:'RazorPay Res Error'
                })
            }
        } return res.send({
            status:'err',
            reason:'Query Failed to Insert'
        })

        
    }else{
        return res.send({
            status:'err',
            reason:'Required Parameter is Missing'
        })
    }

})

export default router;