import { Router } from 'express';
const router = Router();

import enq from './enquiry';
router.use('/enquiry',enq);

export default router;