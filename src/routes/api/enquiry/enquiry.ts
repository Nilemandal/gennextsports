import { Router ,Request ,Response } from "express";
import { Queries } from "../../../shared/queries";
import {SMS} from "../../../models/sms";

const sms = new SMS();

const router = Router();
const query = new Queries();

router.post('/', async (req:Request,res:Response) =>{
    const { id , name , phone, pincode , state , email , enqType } = req.body;
    if(name && phone){
        const data = {
            name:name,
            phone:phone,
            pincode:pincode,
            state:state,
            email:email,
            reference_id:id,
            enq_type:enqType,
            timestamp:+new Date()
        }
        const table = 'enquiry';
        let insertRes:any = await query.insertQuery(data,table);
        if(insertRes.insertId > 0){
            let content = `Hi ${name}, We have received your Request, You can contact us on 8169576951. Generation Next Sports Club.`
            let variable = [ name , '8169576951' ];
            sms.sendNotification({content,phone,type:'Enquiry',variable,template:'enquiry'});

            res.json({
                status:'success',
                data:insertRes.insertId
            })
        }else{
            res.json({
                status:'err',
                msg:'Seomthing went wrong at query End'
            })
        }
            
    }else{
        res.json({
            status:'err',
            msg:'something is missing'
        })
    }
})

export default router;