import { Router,Request,Response } from 'express';
const router = Router();

import registerAdmin from './register';
import adminLogin from './login';
import adminLogout from './logout';

router.use('/',registerAdmin);
router.use('/',adminLogin);
router.use('/',adminLogout)

export default router;