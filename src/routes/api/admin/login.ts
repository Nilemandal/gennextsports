import {Router , Request , Response} from 'express';
import { Queries } from '../../../shared/queries';
import { Hash } from '../../../models/hash';
import jwt from 'jsonwebtoken';
import Env from '../../../env';

const router = Router()
const queries = new Queries();
const hash = new Hash();

/**
 * API is use to login admin n create session for admin
 * Route to test api http://localhost:3001/api/login-admin
 * @param email
 * @param password
 */

router.post('/login-admin', async (req:Request,res:Response)=>{
    let {email ,password } = req.body;
    
    if( email && password){
        let passHash =  (await hash.md5(password));
        var column:any = `aid,admin_name,admin_phone,admin_email,auth_key,admin_role,admin_dept,timestamp,admin_status,pin_code`;

        var conditionObj:any = `WHERE admin_email = '${email}' AND password = '${passHash}'`;

        let queryRes:any = (await queries.selectQuery(column,'admin',conditionObj));
        let d:any =  queryRes[0];
        var session:any = req.session;
        if( d && d.aid){
            session.admin = {
                'aid':d.aid,
                'name':d.admin_name,
                'phone':d.admin_phone,
                'email':d.admin_email,
                'auth':d.auth_key,
                'role':d.admin_role,
                'dept':d.admin_dept,
                'pincode':d.pincode,
            } 
        }
        if(session.admin.aid){
            const payload :any= {
                'aid':d.aid,
                'name':d.admin_name,
                'phone':d.admin_phone,
                'email':d.admin_email
            };
            const privateKey:string = Env.secretKey;
            var jsonToken:any = ''; 
            // jwt reg token
            jwt.sign(payload,privateKey,{expiresIn:Env.tokenTime},(err,token)=>{
                if(!err){
                    jsonToken = token;
                    session.adminToken = token;
                    res.send({'status':'success','token':token,'data':session.admin})
                }else res.json({
                        status:'err',
                        msg:'admin registereted , err while generating token'
                    });
                // checkToken(token);                
            })

            // res.send({'status':'success','data':session.admin})
        }else res.send({'status':'err','msg':'something went wrong at query end'});
            
    }else{
        res.send({'status':'err','msg':'email || password is missing'})
    }

    // function to decode token n verify
    function checkToken(token:any){
        jwt.verify(token,Env.secretKey,(err:any,authData:any)=>{
            console.log(authData);
        });
    }

});

export default router;