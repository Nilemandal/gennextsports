import {Router , Request , Response} from 'express';
import {authJWT} from '../../../middleware/jwtAuth';

const router = Router();

/**
 * API is use to logout n destroy admin session
 * Route to hit API http://localhost:3001/api/logout-admin
 * created by Nile
 */

router.post('/logout-admin', [authJWT] , async (req:any,res:Response)=>{
    var sess:any = req.session;
    req.token = '';
    sess.admin = {};
    res.send({'status':'success'})
});

export default router;
