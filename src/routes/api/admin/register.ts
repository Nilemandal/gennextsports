import {Router , Request , Response} from 'express';
import jwt from 'jsonwebtoken';
import { Queries } from '../../../shared/queries';
import { Hash } from '../../../models/hash';
import Env from '../../../env';
const router = Router()

const queries = new Queries();
const hash = new Hash();

/**
 * API is use to register admin 
 * Route to hit API http://localhost:3001/api/register-admin
 * @param name
 * @param phone
 * @param email 
 * @param password
 */

router.post('/register-admin', async (req:any,res:Response)=>{
    let { name , phone , email , role , pinCode , password , dept , status} = req.body;

    if(name && phone && email && password){
        let hashRes =  (await hash.md5(password));
        var auth_key = (await hash.md5(phone+'|'+email));

        var data:any = {
            'admin_name':`${name}`,
            'admin_phone':`${phone}`,
            'admin_email':`${email}`,
            'auth_key':`${auth_key}`,
            'password':`${hashRes}`,
            'admin_role':`${role}`,
            'admin_dept':`${dept}`,
            'timestamp':+new Date(),
            'admin_status':`${status}`,
            'pin_code':`${pinCode}`
        };
        let queryRes:any = (await queries.insertQuery(data,'admin'));
        // ()?
        if(queryRes.affectedRows){
            const payload :any= {
                'aid':queryRes.insertId,
                'name':name,
                'phone':phone,
                'email':email
            };
            const privateKey:string = Env.secretKey;
            var jsonToken:string = ''; 
            // jwt reg token
            jwt.sign(payload,privateKey,{expiresIn:Env.tokenTime},(err,token)=>{
                if(!err){
                    req.session['adminToken'] = token;
                    res.send({'status':'success','token':token,'data':data});
                }else{
                    res.json({
                        status:'err',
                        msg:'admin registereted , err while generating token'
                    });
                }                
            })

        }else res.send({'status':'err','msg':'something went wrong at query end'});
    }else{
        res.send({'status':'err','msg':'name || phone is missing'})
    }

});

export default router;