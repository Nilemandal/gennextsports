import { Router,Request,Response } from 'express';
const router = Router();

import addBatches from './add_batches';
import bacthDetail from './get_batch_detail';
import getActiveUser from "./get_active_user";
import updatePerformance from "./update_batch_performance";

router.use('/',addBatches);
router.use('/',bacthDetail);
router.use('/get-active-cricket-user',getActiveUser);
router.use('/update-batch-performance',updatePerformance)

export default router;