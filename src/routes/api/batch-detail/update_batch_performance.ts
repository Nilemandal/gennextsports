import { Router , Request , Response } from "express";
import { Queries } from "../../../shared/queries";
import DateUtil from "../../../utils/date";

const router = Router();
const queries = new Queries();

router.post('/', async (req:Request,res:Response) => {
    const { performance , batchProcessId , month } = req.body;
    if(batchProcessId){
        let table = 'batch_process';
        let condition = `WHERE bpid = ${batchProcessId}`;
    
        const getOlderPerformance = await queries.selectQuery('performance',table,condition); 
        const oldPerformance = JSON.parse(getOlderPerformance[0].performance);

        let keyName = DateUtil.monthNames[month]+'_'+new Date().getFullYear();
        let obj:any = new Object();

        if( getOlderPerformance[0] && Object.keys(oldPerformance).length > 0){
            let oldObj = JSON.parse(getOlderPerformance[0].performance);
            obj = { ...oldObj };
        }
        let { batting , bowling , fielding , attendance } = JSON.parse(performance);
        obj[keyName] = {
            "batting":batting,
            "bowling":bowling,
            "fielding":fielding,
            "attendance":attendance
        };

        const updatePerformance:any = await queries.updateQuery({performance:JSON.stringify(obj)},table,condition);
        if(updatePerformance.affectedRows){
            res.send({
                status:'success'
            })
        }else{
            res.send({
                status:'err',
                reason:'Err While Update Try Again!!'
            })
        }
    }else{
        res.send({
            status:'err',
            reason:'Batch Process Id is empty'
        })
    }
})

export default router;