import {Router , Request , Response} from 'express';
import { Queries } from '../../../shared/queries';
const router = Router()

const queries = new Queries();

/**
 * API to insert the batch data 
 * @param batchName 
 * @param batchFees 
 * @param batchCapacity 
 * @param batchStatus
 * API can access through http://localhost:3001/api/add-batches-detail
 * Created By Nile 12/01/2021
 */

router.post('/add-batches-detail', async (req:Request,res:Response)=>{
    let { 
            batchName,
            startTime,
            endTime, 
            weekDays,
            batchFees,
            batchCapacity,
            fitnessDetail,
            batchStatus,
            batchDelete  
        } = req.body;

    
    if(batchName && batchFees && batchCapacity && batchStatus){
        var data:any = {
            'batch_name':batchName,
            'start_time':startTime,
            'end_time':endTime,
            'weekdays':weekDays,
            'batch_fees':batchFees,
            'batch_capacity':batchCapacity,
            'added_by':'Nile',
            'batch_status':batchStatus,
            'fitness_detail':fitnessDetail,
            'batch_delete':0
        }
        const table:string = 'batch_detail';
        let insert = (await queries.insertQuery(data,table));
        if(insert.affectedRows == 1){
            res.send({'status':'success','insertId':insert.insertId});
        }else res.send({'status':'error','msg':'error at query end'});
    }else{
        res.send('oops something is missing');
    }

});

export default router;