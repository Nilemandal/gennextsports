import { Router , Request , Response } from "express";
import {Queries} from "../../../shared/queries";

const router = Router();
const queries = new Queries();

router.post('/' , async (req:Request,res:Response) => {
    let { start , end } = req.body;
    let today = +new Date();
    const column = `bpid , batch_name , batch_id , timestamp , batch_expires , batch_fees , user_phone , user_id , performance`;
    const table = `batch_process`;
    const condition = ` WHERE ${today} BETWEEN timestamp AND batch_expires`;
    let getData:any = await queries.selectQuery(column,table,condition);

    let newData = [];
    let userColumn = 'user_name,user_phone,user_email,user_img';
    for(let i = 0 ; i < getData.length; i++){
        let userCondition = `WHERE uid = ${getData[i].user_id}`;
        let user = await queries.selectQuery(userColumn,'user',userCondition);
        if(user[0]){
            newData.push({
                name:user[0].user_name,
                phone:user[0].user_phone,
                email:user[0].user_email,
                img:user[0].user_img,
                batchProcessId:getData[i].bpid,
                batchId:getData[i].batch_id,
                batchName:getData[i].batch_name,
                batchFees:getData[i].batch_fees,
                batchStartDate:getData[i].timestamp,
                performance:getData[i].performance
            })
        }
    }
    res.send({status:'success',data:newData});
})

export default router