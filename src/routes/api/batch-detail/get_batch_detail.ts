import {Router , Request , Response} from 'express';
import { Queries } from '../../../shared/queries';

const router = Router()
const queries = new Queries();

/**
 * API to get the batch data 
 * API can access through http://localhost:3001/api/get-batches
 * Created By Nile 12/01/2021
 */

router.get('/get-batches', async (req:Request,res:Response)=>{
    const column:string = `bid,batch_name,start_time,end_time,weekdays,batch_fees,batch_capacity,fitness_detail,added_by,batch_status,batch_delete`;
    const table:string = `batch_detail`;
    const condition:string = ``;
    let batchRes:any = (await queries.selectQuery(column,table,condition));
    (batchRes[0] && batchRes[0].bid)?
        res.send({status:'success',data:batchRes}):
        res.send({status:'err','msg':"something went wrong"})
});

export default router;