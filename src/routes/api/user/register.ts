import { Router , Request , Response } from 'express'
import {Hash} from '../../../models/hash';
import {Queries} from '../../../shared/queries';
import Env from '../../../env';
import jwt from 'jsonwebtoken';
import { SMS } from "../../../models/sms";

const router = Router();
var hashKey = new Hash();
var queries = new Queries();
const sms = new SMS();

/**
 * @url http://localhost:3001/api/user/registration
 * @param name
 * @param phone
 * @param email
 * @param dob
 * @param password
 * @param status
 * @res user registration n token session createion for user
 * @author Nilesh Mandal 
 * @Date 22/01/2021
 */

router.post('/registration', async (req:Request,res:Response) => {
    var { 
            name,
            phone,
            email,
            password,
            otp,
            status
        } = req.body;
    let sessOtp:any = req.session;
    
    let column = `uid`;
    let condition = `WHERE user_phone = '${phone}' || user_email = '${email}'`;
    let checkUser = await queries.selectQuery(column,'user',condition);
    if( checkUser[0] && checkUser[0].uid ){
        return res.send({status:'err',code:1,msg:'User Already Register Sign in to Proceed'});
    }

    if(name && password && (phone || email) && otp == sessOtp.otp){
        var hashPass:any = await hashKey.md5(password);
        const CURRENT_MILLIS = +new Date();
        var auth:string = await hashKey.md5(`${phone}|${CURRENT_MILLIS}`);

        var data:any = {
            user_name:name,
            user_phone:phone,
            user_email:email,
            auth_key:auth,
            password:hashPass,
            //user_status:status,
            otp:otp,
            timestamp:CURRENT_MILLIS
        }
        const table:string = `user`;
        var insertUser:any = await queries.insertQuery(data,table);
        if(insertUser.insertId > 0){
            var userSession:any = req.session;

            const payload:any = {
                uid:insertUser.insertId,
                name,
                phone,
                email
            }

            sessOtp['otp'] = '';

            jwt.sign(payload,Env.secretKey,{expiresIn:Env.tokenTime} , async (err:any,token:any) => {
                if(!err){
                    userSession['user'] = {
                        uid:insertUser.insertId,
                        name,
                        phone,
                        email,
                        status:'active'
                    }
                    userSession['userToken'] = token;
                    let content = `Welcome ${name}, Thank you to be member of Generation Next Sport Club. Contact for any query .${Env.supportNumber}`;
                    let variable:any = [ name , Env.supportNumber ];
                    //sms.sendNotification({type:'Notification' , phone , content , variable , template:'registrationAcknowlegment'});

                    res.json({
                        status:'success',
                        token:token,
                        data:userSession.user
                    });
                }else {
                    return res.json({
                                status:'err',
                                msg:'something went wrong at token side'
                            })
                }
            });

        }else res.json({"status":"err","msg":"something went wrong at query side"});
        
    }else{
        res.json({
            'status':'err',
            'msg':'something is missing'
        });
    }
});

export default router;