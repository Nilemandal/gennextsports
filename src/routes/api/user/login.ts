import {Router , Request , Response} from 'express';
import { Queries } from '../../../shared/queries';
import { Hash } from '../../../models/hash';
import jwt from 'jsonwebtoken';
import Env from '../../../env';

const router = Router()
const queries = new Queries();
const hash = new Hash();

/**
 *  @url http://localhost:3001/api/user/check-user
 *  @param phone
 *  @param email
 *  @param option it will be password || otp
 *  @method post
 *  @response otp send to mob number 
 *  @author Nilesh Mandal 
 *  @date 22/01/2021
 */
router.post('/check-user', async (req:Request,res:Response) => {
    let {phone , email , option} = req.body;

    if(phone || email){
        const col:string = `uid,user_name,user_phone,user_email,user_status`;
        const table:string = `user`;
        const condition:string = (phone)?`WHERE user_phone=${phone}`:`WHERE user_email='${email}'`;
        let user:any = await queries.selectQuery(col,table,condition);
        if(user[0].uid){
            if(option != 'password'){
                let session:any = req.session;
                var otp:number;
                if(session['otp']){
                    otp = session['otp'];
                }else {
                    otp = Math.floor(1000 + Math.random() * 9000);
                    session['otp'] = otp;
                }
                console.log('console the otp 39', otp);
                //sms code to send otp to user.
            }

            res.json({
                status:'success',
                code:3
            })
        }else{
            res.json({
                status:'err',
                code:2,
                msg:'user not found'
            })
        }
        
    }else{
        res.json({
            status:'err',
            code:1,
            msg:'phone || email missing'
        })
    }
});

/**
 * @url http://localhost:3001/api/user/login
 * @param phone
 * @param email
 * @param password 
 * @param otp
 * @method POST
 * @response jwt token n user session create
 * @author Nilesh Mandal 
 * @Date 22/01/2021
 */

router.post('/login', async (req:Request,res:Response)=>{
    let {phone,email,password,otp} = req.body;
    let session:any = req.session;
    const column:string = `uid,user_name,user_phone,user_email,auth_key,user_status,timestamp`; 
    const table:string = `user`;
    const phoneEmail:string = (phone)?`user_phone=${phone}`:`user_email='${email}'`;
    
    if( (phone || email) && (password || otp) ){
        let checkUser = await queries.selectQuery(column,table,`WHERE user_phone = ${phone}`);
        if(!checkUser[0]){
            return res.send({status:'err',code:1,msg:'Phone is Not Register, Sign Up to Proceed'});
        }

        if(password){
            const passHash:string = await hash.md5(password);
            const condition:string = `WHERE ${phoneEmail} AND password='${passHash}'`;
            let getUser:any = await queries.selectQuery(column,table,condition);
            if(getUser[0] && getUser[0].uid){

                session['user'] = getUser[0];
                const payload:any = {
                    uid:getUser[0].uid,
                    name:getUser[0].user_name,
                    phone:getUser[0].user_phone,
                    email:getUser[0].user_email
                }
                
                let jwtToken:any = await getToken(payload); 
                if(jwtToken){
                    session['userToken'] = jwtToken;
                    res.json({
                        status:'success',
                        token:jwtToken,
                        data:getUser[0]
                    })
                }else{
                    res.json({
                        status:'err',
                        msg:'something went while generating token'
                    })
                }
            }else res.json({
                            status:'err',
                            msg:'password mismatch'  
                        })
        }else{
            if(session['otp'] == otp){
                const condition:string = `WHERE ${phoneEmail}`; 
                let getUser:any = await queries.selectQuery(column,table,condition);
                if( getUser[0] && getUser[0].uid){
                    session['user'] = getUser[0]
                    const payload:any = {
                        uid:getUser[0].uid,
                        name:getUser[0].user_name,
                        phone:getUser[0].user_phone,
                        email:getUser[0].user_email
                    }

                    session['otp'] = '';
                    let jwtToken:any = await getToken(payload);
                    if(jwtToken){
                        session['userToken'] = jwtToken;
                        res.json({
                            status:'success',
                            token:jwtToken,
                            data:getUser[0]
                        })
                    }else{
                        res.json({
                            status:'err',
                            code:2,
                            msg:'Token Error'
                        })
                    }
                    
                }else{
                    res.json({
                        status:'err',
                        code:1,
                        msg:'User Not Found'
                    })
                }
                
            }else{
                res.json({
                    status:'err',
                    msg:'OTP Mismatched'
                })
            }
            
        }
    }else{
        res.send({'status':'err','msg':'Phone || Password is Missing'})
    }


});

function getToken(payload:any) {
    return new Promise( async (resolve,rejects) =>{
        jwt.sign(payload,Env.secretKey,{expiresIn:Env.tokenTime} , async (err,token) => {
            (!err)?resolve(token):resolve('');
        });
    }); 
}

export default router;