import { Router , Request , Response } from "express";
import {Hash} from "../../../models/hash";
import { Queries } from "../../../shared/queries";
import { SMS } from "../../../models/sms";

const hash = new Hash();
const queries = new Queries();
const sms = new SMS();

const router = Router();
router.post('/' , async (req:Request,res:Response)=>{
    const { user_name , user_phone , user_email , timestamp , otp , password , user_status , uid} = req.body;
    let session:any = req.session;
    if( Number(otp) !== Number(session.otp) ){
        sms.sendOtp({user_phone,otpNumber:session.otp,type:'OTP'});
        console.log( 'session otp' , session['otp'] );
        
        return res.send({
            status:'err',
            code:1,
            reason:'OTP Miss Match'
        })
    }

    let obj:any = new Object();
    if(password){
        const passHash:string = await hash.md5(password);
        obj['password'] = passHash;
    }

    obj = {
        user_name,
        user_phone,
        user_email,
    }

    const table = 'user';
    const condition = `WHERE uid = ${uid}`;
    let updateProfile:any = await queries.updateQuery(obj,table,condition);
    if(updateProfile.affectedRows){
        delete session['otp'];

        session['user'] = {
            uid : uid,
            user_name,
            user_phone,
            user_email,
            user_status:'',
            timestamp:+new Date()
        }

        res.send({
            status:'success'
        })
    }else{
        res.send({
            status:'err',
            reason:'DB Connection Error try again'
        })
    }
})

export default router;