import {Router , Request , Response} from 'express';
const router = Router();

/**
 * API is use to logout n destroy admin session
 * Route to hit API http://localhost:3001/api/user/logout
 * created by Nile on 22/01/2021
 */

router.post('/logout' , async (req:any,res:Response)=>{
    var sess:any = req.session;
    req.userToken = '';
    sess.user = {};
    res.send({'status':'success'})
});

export default router;
