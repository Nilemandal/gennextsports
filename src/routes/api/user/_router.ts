import { Router } from 'express';
import {authJWT} from '../../../middleware/jwtAuth';
import registration from './register';
import login from './login';
import logout from './logout';
import userData from './getUserData';
import profileUpdate from "./profileUpdate";

const router = Router();

router.use('/user',registration);
router.use('/user',login);
router.use('/user',[authJWT],logout);
router.use('/user/data',[authJWT],userData)
router.use('/user/profile-update',[authJWT],profileUpdate)

export default router;