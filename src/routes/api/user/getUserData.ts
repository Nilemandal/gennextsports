import { Router , Request , Response } from "express";
const router = Router();

router.post('/', async ( req:Request, res:Response ) => {
    let session:any = req.session;
    let user = session.user
    if(user.uid){
        console.log( 'console to provide user session 8' );
        delete user['auth_key'];
        res.send({
            status:'success',
            data:user
        })
    }else{
        res.send({
            status:'err',
            reason:'Session Not Found'
        })
    }
})

export default router;
