import { Router , Request , Response } from "express";
import {Queries} from "../../shared/queries";
const router = Router();
const query = new Queries();

/**
 * API to get the Testimonial data 
 * API can access through http://localhost:3001/api/get-testimonial
 * Created By Nile 27/06/2021
 */


router.get('/', async ( req:Request , res:Response )=>{
    const column = 'testimonial_id,cus_name,cus_ratings,cus_insta,cus_feedback,cus_profession,category,timestamp';
    const table = 'testimonials';
    const condition = `WHERE display = 'true'`
    let getTestimonial = await query.selectQuery(column,table,condition);
    res.json({
        status:'success',
        data:getTestimonial
    });
})

export default router;