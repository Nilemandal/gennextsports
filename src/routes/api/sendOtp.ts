import { Router , Request , Response } from "express";
import { SMS } from "../../models/sms";
import { Queries } from "../../shared/queries";
const router = Router();
const sms = new SMS();
const queries = new Queries();

router.post('/send-otp', async (req:Request,res:Response) => {
    let {phone , email , signIn} = req.body;

    let column = `uid`;
    let condition = `WHERE user_phone = '${phone}' || user_email = '${email}'`;
    let checkUser = await queries.selectQuery(column,'user',condition);

    if( checkUser[0] && checkUser[0].uid && !signIn ){
        return res.send({status:'err',code:1,msg:'User Already Register with Phone OR Email, Sign in to Proceed'});
    }else if(signIn && !checkUser[0]){
        return res.send({status:'err',code:2,msg:'Phone is Not Register, Sign Up to Proceed'});
    }

    if( (/^\d{10}$/).test(phone) ){
        let session:any = req.session;
        if(session.otp){
            sms.sendOtp({phone,otpNumber:session.otp,type:'OTP'});
        }else{
            const otp = Math.floor(1000 + Math.random() * 9000);
            session['otp'] = otp;
            sms.sendOtp({phone,otpNumber:session.otp,type:'OTP'});
        }
        console.log( 'session otp' , session['otp'] );
        res.send({
            status:'success',
            msg:'otp sent successfully'
        })
    }else{
        return res.send({status:'err',msg:'phone number length mismatch'});
    }
})

router.post('/send-otp-phone', async (req:Request,res:Response) => {
    let phone = req.body.phone;
    let session:any = req.session;
    if( (/^\d{10}$/).test(phone) ){
        const otp = Math.floor(1000 + Math.random() * 9000);
        session['otp'] = otp;
        sms.sendOtp({phone,otpNumber:session.otp,type:'OTP'});
        console.log( 'session otp' , session['otp'] );
        res.json({
            status:'success'
        })
    }else{
        res.send({
            status:'err',
            reason:'Phone Number is Incorrect OR Missing'
        })
    }
});

export default router;