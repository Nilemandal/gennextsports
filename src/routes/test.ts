import {Router , Request , Response} from 'express';
import { Queries } from '../shared/queries';
const router = Router()

const queries = new Queries();

router.get('/get-db-data', async (req:Request,res:Response)=>{
    let d:any = (await queries.getTestData());
    res.send('db data');
});

export default router;