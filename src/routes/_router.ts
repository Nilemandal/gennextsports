import { Router,Request,Response } from 'express';
const router = Router();

import test from './test';
import batches from './api/batch-detail/_router';
import adminCredential from './api/admin/_router';
import userCredential from './api/user/_router';
import tourActivity from './api/tour-detail/_router';
import enquiry from './api/enquiry/_router';
import sendOtp from "./api/sendOtp";
import payments from './api/payments/_router';
import dashboard from "./api/dashboard/_router";
import testimonial from "./api/testimonial";

router.use('/',test);//test
router.use('/api',batches); // batches 
router.use('/api',adminCredential) //admin
router.use('/api',userCredential) //user
router.use('/api',tourActivity)//tour detail
router.use('/api',enquiry);
router.use('/api',sendOtp);
router.use('/api',payments)
router.use('/api/dashboard',dashboard) // user Dashboard
router.use('/api/get-testimonial',testimonial)

export default router;