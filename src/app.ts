import cookieParser from 'cookie-parser';
import express, {Application, Request,Response} from 'express';
import * as bodyParser from "body-parser";
import helmet from "helmet";
import session from 'express-session';
import Env from './env';
import cors from "cors";
import Razorpay from 'razorpay';
import baseRouter from './routes/_router';
import path from 'path';

const app:Application = express();
var MySQLStore = require('express-mysql-session')(session);
var sessionStore = new MySQLStore(Env.mainDb);

const razorpay = new Razorpay({
    key_id:Env.razorpayId,
    key_secret:Env.razorpayKey
})

var allowlist = ['http://localhost:3000'];
var corsOptionsDelegate = async (req:any,callback:any) =>{
    var corsOptions;
    if(allowlist.indexOf(req.header('Origin')) !== -1 ){
        corsOptions = { origin : true };
    }else{
        corsOptions = { origin : false };
    }
    callback(null,corsOptions);
}
app.use(cors(corsOptionsDelegate));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret:'anySecret',store: sessionStore, saveUninitialized: true,resave: true}));
app.use(express.json());
app.use(helmet());
app.use(bodyParser.json({limit: '200mb'}));
app.use(express.urlencoded({extended: true, limit: 5e7}));
app.use(cookieParser());

app.get('/', async (req:Request,res:Response)=>{
    res.send(`Welcome to Generation Next Sports <br> You are at API Side`);
});

app.use('/',baseRouter)

const port = Env.PORT || 3001;
app.listen(port, ()=>{
    console.log(`server running at http://localhost:${port}`);
})