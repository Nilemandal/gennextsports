export default class DateUtil{
    static monthNames = [
        "January", 
        "February", 
        "March", 
        "April", 
        "May", 
        "June",
        "July", 
        "August", 
        "September",
        "October",
        "November", 
        "December"
    ];

    static monthStartEndDay = (timestamp:any) => {
        let month = timestamp.getUTCMonth();
        let year = timestamp.getUTCFullYear();

        let maxDaysMonth = [0,2,4,6,7,9,11];
        let days = maxDaysMonth.includes(month)?31:30;
        days = (month === 1)?28:days;

        return { startTime:+new Date(year,month,1) , endTime:+new Date(year,month,days) }
    }
}