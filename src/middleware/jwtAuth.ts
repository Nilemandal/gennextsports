import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import Env from '../env';

export const authJWT = (req: Request, res: Response, next: NextFunction) => {
    let headerAuth:any = req.headers['authorization'];
    if(headerAuth){
        var payload:any; 
        
        jwt.verify(headerAuth,Env.secretKey,(err:any,authData:any)=>{
            if(err){
                var sess:any = req.session;
                sess.user = {};
            }else{
                payload = authData
            }
        });

        let session:any = req.session;
        if(  ( session.user && !session.user.uid ) || ( session.admin && !session.admin.aid ) ){
            let logout = [  ];
            if(!session.user){
                logout.push('user')
            }
            if(!session.admin){
                logout.push('admin')
            }

            return res.json({
                status:'err',
                msg:'Session Not Found',
                task:'logout',
                data:logout
            })
        }

        if( session.user && ( payload.uid == session.user.uid)){
            //for user logout
            next();
        }else if(payload.aid && session.admin && (payload.aid == session.admin.aid) ){
            //for admin logout
            next();
        }else{

            let logout = [  ];
            if(!session.user){
                logout.push('user')
            }
            if(!session.admin){
                logout.push('admin')
            }

            return res.json({
                status:'err',
                reason:'Session Expired',
                task:'logout',
                data:logout
            });
        }
    }else{
        return res.json({
            'status':'err',
            'msg':'Header Token Missing'
        })
    }
    // next():
}