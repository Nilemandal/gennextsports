import {Pool} from '../models/pool';
import Env from '../env';

/**
 * File is use to store common query so we can cod once n access as we need
 */

export class Queries{
    dbConnect = new Pool(
        Env.mainDb.host,
        Env.mainDb.user,
        Env.mainDb.password,
        Env.mainDb.database,
    )
    
    /**
     * Test Query Ignore
     */
    getTestData():Promise<any>{
        return new Promise(async (resolve,rejects)=>{
            var data = (await this.dbConnect.queryAsync(`
                            SELECT 
                                * 
                            FROM
                                dummy
                        `));
            resolve(data);
        })
    }

    /**
     * Query to insert data in table
     * @param data => json data need to send key is for col name n value is to insert
     * @param table => table name in which u need to store data
     */
    insertQuery(data:any,table:any):Promise<any>{
        return new Promise ( async (resolve,rejects)=>{
            const keys = Object.keys(data);
            const query:string = `
                            INSERT INTO 
                                ${table} 
                                    (${keys.join(',')}) 
                                VALUES 
                                    (${keys.map(k=>`'${data[k]}'`).join(',')})
                        `;
            var res = (await this.dbConnect.queryAsync(query));
            resolve(res);
        })
    }

    /**
     * Query to select data from DB
     * @param column  => column name in string (a,b,c) to select
     * @param table  => table name from where need to collect data
     * @param condition => condition if required in string 
     */
    selectQuery(column:any,table:any,condition:any):Promise<any>{
        return new Promise(async (resolve,rejects)=>{
            const query = `
                            SELECT 
                                ${column}
                            FROM
                                ${table}
                                ${condition}
                            `;
            //console.log('console at query 67', query );
            var res = (await this.dbConnect.queryAsync(query));
            resolve(res);
        })
    }

    /**
     * Query to update in table
     * @param data should be object key = column name & value is update value
     * @param table 
     * @param condition 
     */
    updateQuery(data:any,table:string,condition:string){
        return new Promise( async (resolve,rejects) =>{
            let updateColVal:any = [];
            for( let key in data ){
                updateColVal.push( `${key} = '${data[key]}'` );
            }
            updateColVal = updateColVal.join(',');
            const query = `
                            UPDATE
                                ${table}
                            SET
                                ${updateColVal}
                                ${condition}
                            `;
            //console.log( 'consle query 93' , query );
            var res = (await this.dbConnect.queryAsync(query));
            resolve(res);
        });
    }
}