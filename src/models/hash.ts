import { Md5 } from 'md5-typescript';

/**
 * @class hash is use to hash the string return
 */

export class Hash {
    md5(str:string):Promise<any>{
        return new Promise((resolve,rejects)=>{
            const res:any = Md5.init(str).toString();
            resolve(res)
        })
    }
}