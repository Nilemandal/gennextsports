import { request } from "https";
import {Pool} from './pool';
import Env from '../env';

export class SMS {
    poolEzo = new Pool(
        Env.mainDb.host,
        Env.mainDb.user,
        Env.mainDb.password,
        Env.mainDb.database
    );
    
    twoFactorKey = '2d8e0e5a-8b8a-11eb-a9bc-0200cd936042';

    async sendSmsRequest(data:any){
        //let smsContent = encodeURIComponent(path);
        let { path , contact , content , timestamp , host , type } = data;
        let smsRes:any = request({
            protocol: 'https:',
            hostname: host,
            port: 443,
            path: encodeURI(path),
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }, res => {
            var serRes = '';
            res.setEncoding('utf8');
            res.on('data', (chunk) => {
                serRes += chunk;
            });
            
            res.on('end',async ()=>{
                var sessionId = '';
                try{
                    sessionId=JSON.parse(serRes).Details;
                    console.log("SMS Send SESSION ID: ",sessionId);
                    let smsInsertQuery = `
                                            INSERT INTO
                                                sms_log
                                                    (
                                                        contact,
                                                        sender,
                                                        session_id,
                                                        content,
                                                        timestamp,
                                                        type
                                                    )
                                                VALUES
                                                    (   
                                                        ${contact},
                                                        '${host}',
                                                        '${sessionId}',
                                                        '${content}',
                                                        ${+new Date()},
                                                        '${type}'
                                                    )
                                            `;
                    (await this.poolEzo.queryAsync(smsInsertQuery));
                }catch(err){
                    console.log( 'Error caugth at sms send 62' , err );
                }
            });            
        });

        smsRes.end();
    }

    async sendOtp(data:any){
        let { otpNumber , phone , type } = data;
        let path = `/API/V1/${this.twoFactorKey}/SMS/${phone}/${otpNumber}/otp`;
        let host = `2factor.in`;
        let content = `${otpNumber} is your OTP. Regards Generation Next Sports Club`;
        //this.sendSmsRequest({path,host,content,type,contact:phone});
    }

    async sendNotification(data:any){
        let { type , phone , content , variable , template } = data;
        let path = `/API/R1/?module=TRANS_SMS&apikey=${this.twoFactorKey}&to=${phone}&from=GNSCIN&templatename=${template}&var1=${variable[0]}&var2=${variable[1]}`;
        let host = `2factor.in`;
        this.sendSmsRequest({path,variable,contact:phone,content,type,host});
    }
    
}

